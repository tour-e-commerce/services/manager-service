# Base image
FROM node:18
# Create app directory

RUN mkdir -p /usr/src/app/certs

# COPY ca.pem /usr/src/app/certs/

WORKDIR /usr/src/app

# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./
COPY prisma ./prisma/
COPY tsconfig.json ./
COPY yarn.lock ./


# ENV DATABASE_URL="postgres://avnadmin:AVNS_GEGVuFz4GPsZhG2Fnsd@tour-tour-ecom.aivencloud.com:14221/defaultdb?sslmode=require&sslrootcert=/usr/src/app/certs/ca.pem"
ENV DATABASE_URL="postgres://avnadmin:AVNS_GEGVuFz4GPsZhG2Fnsd@tour-tour-ecom.aivencloud.com:14221/defaultdb?sslmode=require&sslrootcert=/usr/src/app/certs/-----BEGIN CERTIFICATE-----MIIEQTCCAqmgAwIBAgIUeyTkY2fKEYpMgzTUrC3+9T6dqKUwDQYJKoZIhvcNAQEMBQAwOjE4MDYGA1UEAwwvNzc3Njc4ZDYtMTAzYy00OWRmLTgxZDQtNTk5ZWRmZDk2ZGIwIFByb2plY3QgQ0EwHhcNMjMwNjIyMTQ1NTU4WhcNMzMwNjE5MTQ1NTU4WjA6MTgwNgYDVQQDDC83Nzc2NzhkNi0xMDNjLTQ5ZGYtODFkNC01OTllZGZkOTZkYjAgUHJvamVjdCBDQTCCAaIwDQYJKoZIhvcNAQEBBQADggGPADCCAYoCggGBAJHPFsC4yxxF3h5/lzZ2Kx6P+uLIL73OcBlzZMXnzDkBd30vThAjHa9TGChO6lLgPXnW0/Tzm1BrdziM+T0D/IWWn7/Fx9yLuIE+WOgaKcqZa1P94AMCDlbup5FwS9MglxyEsFrZylpLa/bfelSiqVFYeVmnFILRXw62804WV75rDLpAxbvagniR1vxsIGuz0roNn9pxFQEOeoLSuGgZbavgg6IHBG25hRTT0CYERKV2l+ihXyx3K+rAddlvsEwZWQg/p3lbqBjAVPGjumz1J5TbfUxC/kcKJ72zp0eiR0qotrWKoObl9gv5/wIYuZ+mHBoP+s0Ygpvitf+lfeRZ1gJ4iQlP4mIeftC7SeuLVAgJrZWdw3Uv4r5YOYN32Ys3h4mNIsqd0lJ8aLH8o6DnmHOGwM6SvFryIeaNs1HfbXnyxdOvnJJH7TDUUgtbbgIYJYW7s9iFwUn1pVFnmt9XCH7PKFAhY4xGoomrNOv/Ncf1XnuPJ9IVtP0iXAAp7NaMywIDAQABoz8wPTAdBgNVHQ4EFgQUoUxPjboedvYzVajeWlJJL2TahP8wDwYDVR0TBAgwBgEB/wIBADALBgNVHQ8EBAMCAQYwDQYJKoZIhvcNAQEMBQADggGBAIyg5nrclQdg44ezTKNNugoHaD0/OO6ET0704LrlACSAvzxV+IukcDTz6G3rnHaW8oAVYHqiUyHLLWIgEdIKDMckHU8PDNRlXo/0tWxaikqH3SGHoofAGbfnBVDGiW459UafhOPnFW0CpRvSWNFMyD5ksxm1q1dbxz0bQdUkCgKQif8B4+dMN8vIN4Up5AANQBa2SG88eN599eilMIPKdf3fDnqjTTBxzDsqL0MiBdYlBry4PywL6oi59lxZj+N3JzzkUxKhixYk2TgTY/Jp9wJ9tpAspaounr5nEloTv22THeJpt/tLv40r/EOuD3pglINUstNwRAQnvM4rR50Ao9cVZz5HFcO2fOlEMQG4gHH9Nlp8NptENS28KUOpYoQQBzgKH0itwKNtTfOjXijvNticVfwvLmrUNbgx0TzFs839pTEyI8QZjObW4Hf/5ToTcg32fK4pyu5Hgt4a5HVaGjnXLSHQ8vTsHgzGSUitGX3MNgrKp3ImO62+am3lxs9cuw==-----END CERTIFICATE-----"


# ENV DATABASE_LOCAL="postgres://avnadmin:AVNS_GEGVuFz4GPsZhG2Fnsd@tour-tour-ecom.aivencloud.com:14221/defaultdb?sslmode=require&sslrootcert=./ca.pem"

ENV JWT_AT_SECRET="at-secret"
ENV JWT_RT_SECRET="rt"

# ...

ENV OTP_SECRET = "SECRET"
# Set other environment variables for the application
ENV HOST='smtp.gmail.com'
ENV USER='tranvinh2499@gmail.com'
ENV PASS='aknkpowaaavfsomr'
ENV PROD = "PROD"
ENV PORT=8080
ENV AWS_ACCESS_KEY_ID=AKIA5ZIV7JWRY76P2EFI
ENV AWS_PRIVATE_KEY=ajl5IpEvdJXbn/P84H7l5n/a+iB8L42/ENgwFPYQ
ENV AWS_S3_REGION=ap-southeast-1
ENV AWS_S3_BUCKET=nestjs-upload-practice

ENV FIREBASE_BUCKET = "trading-stuff-67e33.appspot.com"
ENV FIREBASE_PROJECT_ID = "trading-stuff-67e33"
ENV FIREBASE_CLIENT_EMAIL = "firebase-adminsdk-y33lr@trading-stuff-67e33.iam.gserviceaccount.com"
ENV FIREBASE_CLIENT_ID = "102842411324887636421"
ENV FIREBASE_SECRET = "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDT+idOk66leD6c\nWRzIXeI5smpBnKUlY6sPRWAJZpBj1Nn9AO+dO4v1ANQp1cd/xxIBcSxI8VM0hGyk\nmzgbdaPKcEaC8GqN0WJMES2Zr19+85U+P0HyK0KkcqRdQaAb1esXL9tsWBJv5v+E\n0F69TqQbAX0qPji3TzcVYIf6dAalQ8DKnHsUW50hW53G6aOsoyoXP++F5BDPanmh\nrBrhtfg2LG0nSrVqYoPA6HMliu6cUXIa7Kw9n8HfYV/D3kmU3y3AB1NV2zTr/FFd\nR1ZvNdX3H4lCeKmwTT25RvFf4SSHc7zOEkLMyBkJufv2H7Mi/sMQTXO+H62aCm3F\n7fnZTpPVAgMBAAECggEAWZq5uN52id29YavNnekJsuf3QIrtyFNMQOVLhk3DpV+Z\nUfhuUWGVdNiH1rNWZG4K728ekCrqyB1eK2qtmfXtaapyM/FaqV4kFhGA3CEk1e4z\nXR+ySaL9xbPsRLtz6bBQcvm44CCOkpzr0AGgupceiYfkl9G7eclTeIbYlActs/ed\ndEimxaf3aZNCgVs+6z81EzeUklwm9Hob3YFPSVmFf7207Z7SDR3eAv+1zSXguDUd\ny25M6CNl+nOiCMIiEpApfStmNRwdSYm1jzg6hIx5JYRE4rV68JucaUK5vmnplwq0\nhvM1SsKEV4ddj1Jzfyab9I8wky42pXN+v/jU1qTZ9QKBgQD6mVWp1tlFIHbCXidw\nx4VBWIk45hcKZhyTloDnhW0Inql6MTqXLGnXHXX/hZ016E6j3hkXQ/2TN2Po4WRd\nvXegR7g7/PafivW1MJ34rsAXmeHKM67yBAOJOn7bRpf8Ip/9qjf8hY5VnbugJsrp\nBadNkEsVjU03pCUCQkvNzGd1KwKBgQDYi7mwrovE0a0Wbyoxbi0k9r6NNQjKyIym\ndfIJ2oa7Jb7EsGhVpqS/XvcrxNus38I/s4ZQ20ieXH7n9i980KptqOhidiycyxxy\nAk2phRGj187vbLoyMA7Gm8mVU2xdkPfTtqMdDhKUw6VgnjdB4UsfFkh1b/UuK9fV\ngxB9zM+a/wKBgQCeBIQkxUpZsX/RTieWkOkw7FAS5duPfhX05ok/9D1tmwaI+3PT\nkXjn4TbfufQI4wR7hffSWNgTtXC1jzKHfYpqMe81PGyS67/F0fM6tY6BE1Jx+ycM\nioVkSdNmF1yM65RxEp2dCOZYjE6cTPnT9/GoEh2WaHRRapq/kuhTy9lM4wKBgQC+\ncVv0w/1OMm08TrXX1SLE4HBDNcIuvWPdPduIyPQ3AQIeb1rDARTg7T8J8k6UYhGu\n/k8BK2lZRNWjYULwTX+lg6gkd/PSTVsN9XKhxNL3umNW89NpMhS8nOQTfNPQEh+y\nxWv8sJSs2sfGCj9kpCPyCiHdjoIdgheGwoatbIOktwKBgANwr35hVZSb4Vkec0x3\n5ACoC4SO3swqodFNqT0s2FkKV9/OzZiJmRQ02KEIp9zNObJk2fABeN7vWO6Ccmaq\nRrjdiwpWw7mWISLP7uJ/TnmBZqmjI58mmpcDcwo1DMq1XzwNu94pE7BCpE0N2sav\nfvP05yuTmru5rpzT6M1DnJaP\n-----END PRIVATE KEY-----\n"

# Install app dependencies
RUN yarn install
RUN npx prisma generate
# Bundle app source
COPY . .

EXPOSE 8080

# Creates a "dist" folder with the production build
RUN npm run build

# Start the server using the production build
CMD [ "node", "dist/main.js" ]