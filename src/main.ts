import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import { Logger } from '@nestjs/common';
import { ValidationPipe } from '@nestjs/common/pipes';
import { ConfigService } from '@nestjs/config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as moment from 'moment';
import * as morgan from 'morgan';
const newLogger = new Logger('bootstrap');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const appConfig = await app.resolve<ConfigService>(ConfigService);

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidUnknownValues: true,
    }),
  );

  app.use(
    morgan(function (tokens, req, res) {
      const resStatus = tokens.status(req, res);
      const resTime = tokens['response-time'](req, res);
      const reqMethod = tokens.method(req, res);
      const reqUrl = tokens.url(req, res);
      const reqIp =
        req.headers['x-real-ip'] ||
        req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress;
      const reqDate = tokens['date'](req, res);

      const content = `${moment(reqDate).format(
        'YYYY-MM-DD HH:mm:ss',
      )} ${reqIp} ${reqMethod} ${reqUrl} ${resStatus} - ${resTime} ms`;
      newLogger.log(content);
    }),
  );

  ////cors accept all origins
  app.enableCors();

  //create swagger
  const swaggerConfig = new DocumentBuilder()
    .addBearerAuth()
    .setTitle('Dtour-Auth API')
    .setDescription('The Auth API description')
    .setVersion('1.0')

    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('api', app, document);
  //set port
  const port = appConfig.get('PORT') || 3000;

  SwaggerModule.setup('api', app, document);
  //expose application
  await app.listen(port);
  newLogger.log(`-------------------Config-------------------`);
  newLogger.log(JSON.parse(JSON.stringify(appConfig)).cache);
  newLogger.log('-------------------------------------------');
  newLogger.log(
    `Application listening on port ${port} ready to receive requests`,
  );
}
bootstrap();
