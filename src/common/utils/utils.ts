import * as argon from 'argon2';

export async function hashData(data: string) {
  return await argon.hash(data);
}

// export function generateRandomString(length) {
//   const charset =
//     'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
//   let result = '';

//   for (let i = 0; i < length; i++) {
//     const randomIndex = Math.floor(Math.random() * charset.length);
//     result += charset.charAt(randomIndex);
//   }

//   return result;
// }
