import { HttpException, HttpStatus } from '@nestjs/common';

export class ResponseException extends HttpException {
  constructor(dto: {
    message?: string;
    status: HttpStatus;
    errorCode?: string;
    error?: any;
    [key: string]: any;
  }) {
    super(
      {
        status: dto.status,
        message: dto.message,
        error: dto.error,
      },
      dto.status,
      { cause: dto.error },
    );
  }
}
