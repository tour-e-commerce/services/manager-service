import { HttpStatus } from '@nestjs/common';

export class ResponseServer<T> {
  private readonly message: any;
  private readonly status: HttpStatus;
  private readonly data: T;

  constructor(dto: {
    message: string;
    status: HttpStatus;
    data: T;
    [key: string]: any;
  }) {
    this.message = dto.message;
    this.status = dto.status;
    this.data = dto.data;
  }
}
