import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';

export class PaginationDto {
  @ApiPropertyOptional()
  @IsInt()
  take?: number;

  @ApiPropertyOptional()
  @IsInt()
  skip?: number;

  @ApiPropertyOptional()
  @IsString()
  query?: string;
}
