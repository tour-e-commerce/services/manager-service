import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';

export enum TicketMapping {
  ADULT = 1,
  CHILDREN = 2,
}
export enum PricingTypeMapping {
  DEFAULT = 1,
  FREE_WITH_TICKET = 2,
  FREE_NO_TICKET = 3,
}
export enum Ticket {
  ADULT = 'ADULT',
  CHILDREN = 'CHILDREN',
}
export enum PricingType {
  DEFAULT = 'DEFAULT',
  FREE_WITH_TICKET = 'FREE_WITH_TICKET',
  FREE_NO_TICKET = 'FREE_NO_TICKET',
}
export class PriceRange {
  @IsNotEmpty()
  @IsNumber()
  from_amount: number;

  @IsNotEmpty()
  @IsNumber()
  to_amount: number;

  @IsNotEmpty()
  @IsNumber()
  price: number;
}

export class TicketPricingModel {
  @ApiProperty({ enum: Ticket })
  @IsEnum(Ticket)
  ticket_type: Ticket;

  @ApiProperty({ type: String })
  @IsString()
  tour_id: string;

  @ApiProperty({ enum: PricingType })
  @IsEnum(PricingType)
  pricing_type: PricingType;

  @ApiProperty({ type: Number })
  @IsOptional()
  @IsNumber()
  minimum_booking_quantity?: number;

  @ApiProperty({ type: Number })
  @IsOptional()
  @IsNumber()
  maximum_booking_quantity?: number;

  @ApiProperty({ type: PriceRange })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => PriceRange)
  price_range: PriceRange[];
}
