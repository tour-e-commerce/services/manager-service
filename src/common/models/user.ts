export enum GENDER {
  MALE = 'M',
  FEMALE = 'F',
  UNKNOW = 'O',
}

export enum UserOrderBy {
  EMAIL_ASC = 'email:asc',
  EMAIL_DESC = 'email:desc',
  FULL_NAME_ASC = 'full_name:asc',
  FULL_NAME_DESC = 'full_name:desc',
  // Add more enum values for other properties and sorting options as needed
}
