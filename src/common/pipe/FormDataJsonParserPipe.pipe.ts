import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  BadRequestException,
} from '@nestjs/common';

@Injectable()
export class FormDataJsonParserPipe implements PipeTransform<string | any> {
  transform(value: string | undefined, metadata: ArgumentMetadata): any {
    console.log(metadata);
    if (value === undefined) {
      console.log(value);
      return undefined;
    }

    try {
      const parsedValue = JSON.parse(Object.values(value)[0]);
      return parsedValue;
    } catch (error) {
      throw new BadRequestException('Invalid Json Format');
    }

    // return undefined
  }
}
