import { ApiProperty } from '@nestjs/swagger';
import { BlockTypeEnum } from '@prisma/client';
import { IsEmail, IsEnum, IsNotEmpty, IsString } from 'class-validator';

export class VerifyOtpDTO {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  otp: string;

  @IsString()
  @ApiProperty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsEnum(BlockTypeEnum)
  @ApiProperty({
    enum: BlockTypeEnum,
  })
  type: string;
}
