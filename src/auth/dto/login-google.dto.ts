import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class LoginGoogleDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  accessToken: string;
}
