import { Injectable } from '@nestjs/common';
import { Prisma, User } from '@prisma/client';
import { GetUserPaginationDto } from 'src/user/dto/get-user-pagination.dto';
import { ROLE } from '../common/constants/enum';
import { PrismaService } from '../prisma/prisma.service';
import { UpdateUserInputDto } from '../provider/dto/update-user-input.dto';
import { UserDto } from '../user/dto/user.dto';

@Injectable()
export class UserDataAccess {
  constructor(private readonly prisma: PrismaService) {}

  async getUserByEmail(email: string): Promise<User> {
    return await this.prisma.user.findFirst({
      where: {
        email: email,
        role_id: ROLE.PROVIDER,
      },
    });
  }

  async getUserByQuery(query: any): Promise<User> {
    return await this.prisma.user.findFirst(query);
  }
  async getUserByEmailAndRole(email: string, roleId: number): Promise<User> {
    return await this.prisma.user.findUnique({
      where: {
        email_role_id: {
          email: email,
          role_id: roleId,
        },
      },
    });
  }

  async updateUserPassword(userId: string, password: string): Promise<User> {
    return await this.prisma.user.update({
      where: {
        id: userId,
      },
      data: {
        password: password,
      },
    });
  }

  async updateUserProfile(
    userId: string,
    dto: UpdateUserInputDto,
  ): Promise<User> {
    return await this.prisma.user.update({
      where: {
        id: userId,
      },
      data: {
        full_name: dto?.fullName,
        gender: dto?.gender,
        dob: dto.dob ? new Date(dto.dob) : null,
        phone_number: dto?.phoneNumber,
        avatar_image_url: dto?.avatarImageUrl,
        banner_image_url: dto?.bannerImageUrl,
      },
    });
  }

  async getAllUsers(dto: GetUserPaginationDto): Promise<{
    total: number;
    Users: any;
  }> {
    const { skip, take, orderBy, query } = dto;

    const where: Prisma.UserWhereInput = query
      ? {
          OR: [
            {
              email: {
                contains: query,
                mode: 'insensitive',
              },
            },
            {
              full_name: {
                contains: query,
                mode: 'insensitive',
              },
            },
          ],
        }
      : {};

    let prismaOrderBy: Prisma.TourOrderByWithRelationInput | undefined =
      undefined;

    if (orderBy) {
      const [field, order] = orderBy.split(':');
      prismaOrderBy = { [field]: order as 'asc' | 'desc' };
    }

    const users = await this.prisma.$transaction([
      this.prisma.user.count(),
      this.prisma.user.findMany({
        take: take,
        skip: skip,
        where: where,
        orderBy: prismaOrderBy,
        select: {
          id: true,
          avatar_image_url: true,
          banner_image_url: true,
          email: true,
          gender: true,
          full_name: true,
        },
      }),
    ]);

    return {
      total: users[0],
      Users: users[1],
    };
  }

  async getUniqueUserById(userId: string): Promise<User> {
    const user = await this.prisma.user.findUnique({
      where: {
        id: userId,
      },
    });
    return user;
  }

  async updateUserById(userId: string, dto: UserDto): Promise<User> {
    const user = await this.prisma.user.update({
      where: {
        id: userId,
      },
      data: {
        email: dto?.email,
        password: dto?.password,
        full_name: dto?.fullName,
        avatar_image_url: dto?.avatarImageUrl,
        banner_image_url: dto?.bannerImageUrl,
        dob: dto?.dob,
        phone_number: dto?.phoneNumber,
        gender: dto?.gender,
        refresh_token: dto?.rtHash,
        refresh_token_expire: dto?.rtExpired,
      },
    });
    return user;
  }
  // cái này bị sai đừng có dùng
  async getUniqueUser(dto: UserDto): Promise<User> {
    const user = await this.prisma.user.findFirst({
      where: {
        email: dto.email,
        role_id: dto?.roleId,
      },
    });

    return user;
  }

  async createUser(dto: UserDto): Promise<User> {
    return await this.prisma.user.create({
      data: {
        email: dto.email,
        password: dto.password,
        role_id: ROLE.PROVIDER,
        full_name: dto?.fullName,
        avatar_image_url: dto?.avatarImageUrl,
        banner_image_url: dto?.bannerImageUrl,
        dob: dto?.dob,
        phone_number: dto?.phoneNumber,
        gender: dto?.gender,
      },
    });
  }

  async deleteUserRtHash(userId: string): Promise<any> {
    const updateUser = await this.prisma.user.updateMany({
      where: {
        id: userId,
        refresh_token: {
          not: null,
        },
      },
      data: {
        refresh_token: null,
      },
    });
    return updateUser;
  }

  async updateUserPasswordByEmail(dto: UserDto): Promise<User> {
    const check = await this.prisma.user.findFirst({
      where: {
        email: dto.email,
        role_id: dto.roleId,
      },
    });

    return await this.prisma.user.update({
      where: {
        id: check.id,
      },
      data: {
        password: dto.password,
      },
    });
  }

  // async updateUserReportCount(userId: string): Promise<User> {
  //   return await this.prisma.user.update({
  //     where: {
  //       id: userId,
  //     },
  //     data: {
  //       received_report_count: {
  //         increment: 1,
  //       },
  //     },
  //   });
  // }

  // async resetUserReportCount(userId: string): Promise<User> {
  //   return await this.prisma.user.update({
  //     where: {
  //       id: userId,
  //     },
  //     data: {
  //       received_report_count: 0,
  //     },
  //   });
  // }

  async getUserContent(userId: string) {
    return await this.prisma.user.findUnique({
      where: {
        id: userId,
      },
      select: {
        TourReview: true,
        ReplyReview: true,
      },
    });
  }

  async updateUserEmailConfirmed(userId: string) {
    return await this.prisma.user.update({
      where: {
        id: userId,
      },
      data: {
        is_email_confirmed: true,
      },
    });
  }

  async getProviderId(userid: string) {
    return await this.prisma.user.findUnique({
      where: {
        id: userid,
      },
      select: {
        Providers: {
          select: {
            id: true,
          },
        },
      },
    });
  }

  async createNewProviderUser(email: string, secret: string) {
    return await this.prisma.user.create({
      data: {
        email: email,
        role_id: ROLE.PROVIDER,
        otp_secret: secret,
      },
    });
  }

  async createStaffUser(email: string, password: string) {
    return await this.prisma.user.create({
      data: {
        email: email,
        password: password,
        is_email_confirmed: true,
        role_id: ROLE.STAFF,
      },
    });
  }

  async getAllUsersByEmail(email: string) {
    return await this.prisma.user.findMany({
      where: {
        email: email,
      },
    });
  }
}
