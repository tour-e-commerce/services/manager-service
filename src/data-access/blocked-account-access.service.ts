import { Injectable } from '@nestjs/common';
import { BlockedAccount, BlockTypeEnum } from '@prisma/client';
import { ROLE } from '../common/constants/enum';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class BlockedAccountDataService {
  constructor(private readonly prisma: PrismaService) {}

  async getBlockedAccountByEmailAndType(
    email: string,
    type: BlockTypeEnum,
  ): Promise<BlockedAccount> {
    return await this.prisma.blockedAccount.findFirst({
      where: {
        email: email,
        role_id: ROLE.PROVIDER,
        type,
      },
    });
  }

  async createOne(email: string, type: BlockTypeEnum): Promise<BlockedAccount> {
    return await this.prisma.blockedAccount.create({
      data: {
        email: email,
        type,
        role_id: ROLE.PROVIDER,
        otpCount: 0,
      },
    });
  }

  async updateOtpCount(id: string) {
    return await this.prisma.blockedAccount.update({
      where: {
        id: id,
      },
      data: {
        otpCount: {
          increment: 1,
        },
      },
    });
  }

  async updateBlockedAccount(id: string, data) {
    return await this.prisma.blockedAccount.update({
      where: {
        id: id,
      },
      data: data,
    });
  }

  async getOneByEmail(email: string) {
    return await this.prisma.blockedAccount.findMany({
      where: {
        email: email,
      },
    });
  }

  async getBlockedAccountsByEmail(email: string) {
    return await this.prisma.blockedAccount.findMany({
      where: {
        email: email,
      },
    });
  }
}
