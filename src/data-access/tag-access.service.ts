import { Injectable } from '@nestjs/common';
import { Prisma, Tag } from '@prisma/client';
import { PrismaService } from '../prisma/prisma.service';
import { GetAllTagDto } from '../tour/dto/get-all-tag.dto';
@Injectable()
export class TagAccessService {
  constructor(private prisma: PrismaService) {}
  async getAllTag(getAllTagDto: GetAllTagDto): Promise<Tag[]> {
    try {
      if (getAllTagDto?.search || getAllTagDto?.tag_type) {
        const search = getAllTagDto?.search ? getAllTagDto.search : undefined;
        const tagType = getAllTagDto?.tag_type
          ? getAllTagDto.tag_type
          : undefined;
        const where: Prisma.TagWhereInput =
          search || tagType
            ? {
                AND: [
                  search
                    ? { name: { contains: search, mode: 'insensitive' } }
                    : {},
                ],
              }
            : {};
        const tagData = await this.prisma.tag.findMany({
          where: where,
        });

        return tagData;
      }
      // const splitOrderBy = orderBy.split(":");
      const tagData = await this.prisma.tag.findMany({});
      return tagData;
    } catch (error) {
      console.log(error);
      throw new Error('Internal server error');
    }
  }
  getTagById(id: number): Promise<JSON> {
    console.log(id);
    throw new Error('Method not implemented.');
  }
}
