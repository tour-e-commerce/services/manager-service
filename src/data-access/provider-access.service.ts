import {
  HttpStatus,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { Provider, ProviderEnum, ServiceTypeEnum } from '@prisma/client';
import { ResponseException } from 'src/common/interface/exception.interface';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateProviderDto } from 'src/provider/dto/create-provider.dto';
import { UpdateProviderDTO } from 'src/provider/dto/update-provider.dto';

@Injectable()
export class ProviderDataAccess {
  constructor(private readonly prisma: PrismaService) {}
  async getProviderById(providerId: string): Promise<Provider> {
    try {
      return await this.prisma.provider.findFirst({
        where: {
          id: providerId,
        },
      });
    } catch (error) {
      throw new error();
    }
  }

  async updateProvider(
    providerId: string,
    dto: UpdateProviderDTO,
  ): Promise<Provider> {
    return await this.prisma.provider.update({
      where: {
        id: providerId,
      },
      data: {
        description: dto?.description,
        phone: dto.phone,
        email: dto?.email,
        address_name: dto.address_name,
        address_ward: dto.address_ward,
        address_province: dto.address_province,
        address_country: dto.address_country,
      },
    });
  }
  async updateProviderStatus(
    providerId: string,
    status: string,
  ): Promise<Provider> {
    return await this.prisma.provider.update({
      where: {
        id: providerId,
      },
      data: {
        status: ProviderEnum[status as keyof typeof ProviderEnum],
      },
    });
  }

  async createProvider(
    userId: string,
    dto: CreateProviderDto,
  ): Promise<Provider> {
    try {
      const newProvider = await this.prisma.provider.create({
        data: {
          tax_code: dto.taxCode,
          company_name: dto.companyName,
          address_name: dto.address_name,
          address_district: dto.address_district,
          address_ward: dto.address_ward,
          address_province: dto.address_province,
          address_country: dto.address_country,
          description: dto?.description,
          email: dto.email,
          phone: dto?.phone,
          user_id: userId,
          business_license: dto.file,
          service_type:
            ServiceTypeEnum[dto.serviceType as keyof typeof ServiceTypeEnum],
        },
      });
      return newProvider;
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException(error);
    }

    // return await this.prisma.provider.update({
    //   where: {
    //     id: newProvider.id,
    //   },
    //   data: {
    //     phone: {
    //       push: dto?.phone,
    //     },
    //     images: {
    //       push: dto?.images,
    //     },
    //   },
    // });
  }

  async getProviderByUserId(userId: string): Promise<Provider> {
    return await this.prisma.provider.findUnique({
      where: {
        user_id: userId,
      },
    });
  }

  async getProviderByIdWithStatus(
    providerId: string,
    status: string,
  ): Promise<Provider> {
    return await this.prisma.provider.findFirst({
      where: {
        id: providerId,
        status: ProviderEnum[status as keyof typeof ProviderEnum],
      },
    });
  }

  async patchProviderAvatar(
    puid: string,
    avatarURL: string,
  ): Promise<Provider> {
    try {
      const data = await this.prisma.provider.update({
        where: { user_id: puid },
        data: { avatar_image_url: avatarURL },
      });
      return data;
    } catch (error) {
      throw new ResponseException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        error,
      });
    }
  }

  async patchProviderBanner(
    puid: string,
    bannerURL: string,
  ): Promise<Provider> {
    try {
      const data = await this.prisma.provider.update({
        where: { user_id: puid },
        data: { banner_image_url: bannerURL },
      });
      return data;
    } catch (error) {
      throw new ResponseException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error,
      });
    }
  }
}
