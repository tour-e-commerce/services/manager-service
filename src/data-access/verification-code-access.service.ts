import { Injectable } from '@nestjs/common';
// import { VerificationCode } from '@prisma/client';

import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class VerificationCodeDataAccessService {
  constructor(private readonly prisma: PrismaService) {}

  // async getOtpsByEmail(email: string): Promise<VerificationCode[]> {
  //   return this.prisma.verificationCode.findMany({
  //     where: {
  //       email: email,
  //     },
  //     orderBy: {
  //       created_at: 'desc',
  //     },
  //   });
  // }

  // async createOne(data): Promise<VerificationCode> {
  //   return await this.prisma.verificationCode.create(data);
  // }

  // async updateOne(where: any, data: any): Promise<VerificationCode> {
  //   return await this.prisma.verificationCode.update({
  //     where,
  //     data,
  //   });
  // }

  // async cleanCodeByEmail(email: string) {
  //   return await this.prisma.verificationCode.deleteMany({
  //     where: {
  //       email: email,
  //     },
  //   });
  // }
}
