import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class DataAccessService {
  constructor(private readonly prisma: PrismaService) {}

  async getManyWithPagination<T>(
    model: string,
    page: number,
    perPage: number,
    filters: Record<string, any> = {},
    orderBy: Record<string, 'asc' | 'desc'> = {},
  ): Promise<T[]> {
    const skip = (page - 1) * perPage;
    const data = await this.prisma[model].findMany({
      skip,
      take: perPage,
      where: filters,
      orderBy,
    });

    return data;
  }

  async countRecords(
    model: string,
    filters: Record<string, any> = {},
  ): Promise<number> {
    return this.prisma[model].count({ where: filters });
  }

  async getUniqueRecord<T>(
    model: string,
    where: Record<string, any>,
  ): Promise<T> {
    const data = await this.prisma[model].findUnique({
      where: where,
    });
    return data;
  }
}
