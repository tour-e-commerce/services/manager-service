import { Module } from '@nestjs/common';
import { DataAccessModule } from 'src/data-access/data-access.module';
import { UserDataAccess } from 'src/data-access/user-access.service';
import { UtilitiesModule } from 'src/utilities/utilities.module';
import { StaffController } from './staff.controller';
import { StaffService } from './staff.service';

@Module({
  imports: [UtilitiesModule, DataAccessModule],
  controllers: [StaffController],
  providers: [StaffService, UserDataAccess],
})
export class StaffModule {}
