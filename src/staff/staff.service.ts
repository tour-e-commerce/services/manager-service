import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Provider, ProviderEnum } from '@prisma/client';
import { DataAccessService } from 'src/data-access/data-access.service';
import { ProviderDataAccess } from 'src/data-access/provider-access.service';
import { ValidationService } from 'src/utilities/validation.service';

@Injectable()
export class StaffService {
  constructor(
    private readonly providerDataService: ProviderDataAccess,
    private dataAccessService: DataAccessService,
    private validationService: ValidationService,
  ) {}
  async getAllProviders(page: number, select: number, status: string) {
    if (status !== ProviderEnum[status as keyof typeof ProviderEnum])
      return {
        status: 200,
        message: 'get providers successfully',
        data: {
          total: 0,
          providers: [],
        },
      };
    const filter = {
      status: status,
    };
    const count = await this.dataAccessService.countRecords('provider', filter);
    const data = await this.dataAccessService
      .getManyWithPagination<Provider>('provider', page, select, filter)
      .catch(error => {
        throw new InternalServerErrorException(error);
      });
    return {
      status: 200,
      message: 'get providers successfully',
      data: {
        total: count,
        providers: data,
      },
    };
  }

  async setStatusProvider(userId: string, providerId: string, status: string) {
    const provider = await this.dataAccessService.getUniqueRecord<Provider>(
      'provider',
      {
        id: providerId,
      },
    );

    if (!provider) throw new NotFoundException('provider not found');

    if (
      provider.status === ProviderEnum.DISABLED ||
      status === ProviderEnum.DISABLED
    ) {
      await this.validationService.isAdministratorOrThrow(userId);
    }

    if (status === ProviderEnum.PROCESSING)
      throw new BadRequestException(
        'you can not apply this status of provider ',
      );

    const updateProvider = await this.providerDataService
      .updateProviderStatus(providerId, status)
      .catch(error => {
        console.log('error at staff service ln 76');
        throw new InternalServerErrorException(error);
      });
    return {
      status: 200,
      message: 'update provider status success fully',
      data: updateProvider,
    };
  }

  async getProviderDetail(providerId: string) {
    const provider = await this.dataAccessService.getUniqueRecord<Provider>(
      'provider',
      {
        id: providerId,
      },
    );
    if (!provider) throw new NotFoundException('Provider not found');
    return {
      status: 200,
      message: `get provider ${providerId} success`,
      data: provider,
    };
  }
}
