import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AdminModule } from './admin/admin.module';
import { AuthModule } from './auth/auth.module';
import { AvailabilityModule } from './availability/availability.module';
import { DataAccessModule } from './data-access/data-access.module';
import { PrismaModule } from './prisma/prisma.module';
import { ProviderModule } from './provider/provider.module';
import { ResourceModule } from './resource/resource.module';
import { RolesModule } from './roles/roles.module';
import { StaffModule } from './staff/staff.module';
import { TourModule } from './tour/tour.module';
import { UserModule } from './user/user.module';
import { UtilitiesModule } from './utilities/utilities.module';
import { TicketPricingModule } from './ticket-pricing/ticket-pricing.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    PrismaModule,
    AuthModule,
    RolesModule,
    UserModule,
    DataAccessModule,
    ProviderModule,
    UtilitiesModule,
    TourModule,
    ResourceModule,
    AdminModule,
    AvailabilityModule,
    StaffModule,
    TicketPricingModule,
  ],
})
export class AppModule {}
