import { ApiProperty } from '@nestjs/swagger';

import { Type } from 'class-transformer';
import {
  IsArray,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import {
  PriceRange,
  PricingType,
  Ticket,
} from 'src/common/models/ticket-pricing';

export class PricingData {
  @ApiProperty({ enum: Ticket })
  @IsEnum(Ticket)
  ticket_type: Ticket;

  @ApiProperty({ enum: PricingType })
  @IsEnum(PricingType)
  pricing_type: PricingType;

  @ApiProperty({ type: Number })
  @IsOptional()
  @IsNumber()
  minimum_booking_quantity?: number;

  @ApiProperty({ type: Number })
  @IsOptional()
  @IsNumber()
  maximum_booking_quantity?: number;

  @ApiProperty({ type: PriceRange })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => PriceRange)
  price_range: PriceRange[];
}

export class CreateTicketPricingDTO {
  @ApiProperty({ type: String })
  @IsString()
  tour_id: string;

  @ApiProperty({ type: PricingData })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => PricingData)
  pricing_data: PricingData[];
}

export class GetByTourIdDTO {
  @ApiProperty({ type: String })
  @IsString()
  tour_id: string;
}
