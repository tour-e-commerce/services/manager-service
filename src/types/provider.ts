export type Provider = {
  id: string;
  website_url: string;

  description: string;
  phone: string;
  email: string;
  address: string;
  status: string;
};
