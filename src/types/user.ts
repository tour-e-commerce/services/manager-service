export type User = {
  id: string;
  email: string;
  phone_number: string;
  full_name: string;
  password: string;
  dob: Date;
  gender: string;
  avatar_image_url: string;
  banner_image_url: string;
  roleId: number;
  status: string;
  rtHash: string;
};
