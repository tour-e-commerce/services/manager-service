import {
  IsNotEmpty,
  IsString,
  IsNumber,
  IsArray,
  ValidateNested,
  IsOptional,
  IsInt,
  IsEnum,
  ArrayUnique,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { TourLocationTypeEnum, TourSchedule } from '../../common/models/tour';

export class CreateOneTourDTO {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  description: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  footnote: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsArray()
  tour_images?: string[];

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  duration: number;

  @ApiProperty({ type: Array(Number) })
  @IsArray()
  @IsNotEmpty()
  @ArrayUnique()
  tag_id: number[];

  @ApiProperty({ type: Array(Number) })
  @IsArray()
  @IsNotEmpty()
  @ArrayUnique()
  vehicle_id: number[];

  @ApiProperty()
  @IsEnum(TourLocationTypeEnum)
  // @IsNotEmpty()
  tour_location_type: TourLocationTypeEnum;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  address_name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  address_district: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  address_ward: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  address_province: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  address_country: string;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => TourSchedule)
  @ApiProperty({ type: TourSchedule })
  TourSchedule: TourSchedule[];
}
