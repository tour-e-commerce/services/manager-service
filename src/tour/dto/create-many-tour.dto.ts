import { ApiProperty } from '@nestjs/swagger';

export class CreateManyTourDTO {
  @ApiProperty()
  name: string;

  @ApiProperty()
  description: string;

  @ApiProperty()
  footnote: string;

  @ApiProperty()
  price: number;

  @ApiProperty()
  duration_day: number;

  @ApiProperty()
  duration_night: number;

  @ApiProperty()
  location: string;

  @ApiProperty()
  id_tags: number[];
}
