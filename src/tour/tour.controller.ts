import {
  Body,
  Controller,
  FileTypeValidator,
  Get,
  HttpStatus,
  MaxFileSizeValidator,
  Param,
  ParseFilePipe,
  Post,
  Put,
  Query,
  UploadedFile,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { GetCurrentUser } from '../common/decorators/get-current-user.decorator';
import { Roles } from '../common/decorators/roles.decorator';

import { ROLE } from 'src/common/constants/enum';
import { ResponseException } from 'src/common/interface/exception.interface';
import { AtGuard } from '../common/guards';
import { RolesGuard } from '../common/guards/role.guard';
import { FormDataJsonParserPipe } from '../common/pipe/FormDataJsonParserPipe.pipe';
import { CreateOneTourDTO } from './dto/create-one-tour.dto';
import { GetAllTourDTO } from './dto/get-all-tour.dto';
import { TourService } from './tour.service';

const MAX_FILE_SIZE = 1024 * 1024 * 8;
const IMAGE_FILE_TYPE = /(jpg|jpeg|png|webp)$/;

@Controller('tour')
@ApiTags('Tour')
@ApiBearerAuth()
@Roles(ROLE.PROVIDER)
@UseGuards(AtGuard, RolesGuard)
export class TourController {
  constructor(private tourService: TourService) {}

  @Post()
  @UseInterceptors(FilesInterceptor('tour_images'))
  async createOneTour(
    @UploadedFiles(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({
            maxSize: MAX_FILE_SIZE,
          }),
          new FileTypeValidator({
            fileType: IMAGE_FILE_TYPE,
          }),
        ],
      }),
    )
    images: Array<Express.Multer.File>,
    @Body(new FormDataJsonParserPipe()) tourDTO: any,
    @GetCurrentUser('id') userID: string,
  ) {
    console.log(images);
    //validate tour
    const serializedData = plainToClass(CreateOneTourDTO, tourDTO);
    const error = await validate(serializedData);

    if (error.length > 0) {
      const parsedError = error.map(errorItem => {
        return {
          property: errorItem.property,
          constraint: errorItem.constraints,
        };
      });
      throw new ResponseException({
        status: HttpStatus.BAD_REQUEST,
        message: 'Bad Request',
        error: parsedError,
      });
    } else {
      const result = await this.tourService.addOneTour(tourDTO, userID, images);
      return {
        status: HttpStatus.CREATED,
        message: 'Create One Tour Success',
        data: result,
      };
    }
  }
  @Post('files')
  @UseInterceptors(FileInterceptor('file'))
  async uploadCSVFile(@UploadedFile() file: Express.Multer.File) {
    const data = await this.tourService.handleCsv(file);
    return {
      status: HttpStatus.OK,
      message: 'UploadCSVFile by success',
      data: data,
    };
  }
  @Get('/detail/:id')
  async getTourById(@Param() param: { id: string }) {
    const data = await this.tourService.getTourById(param.id);
    return {
      status: HttpStatus.OK,
      message: 'Get Tour by ID success',
      data: data,
    };
  }

  @Get('/provider')
  async getTourByProviderId(
    @GetCurrentUser('id') providerUserID: string,
    @Query() getAllTourDTO: GetAllTourDTO,
  ) {
    console.log(providerUserID);
    const data = await this.tourService.getTourByProviderId(
      getAllTourDTO,
      providerUserID,
    );
    return {
      status: HttpStatus.OK,
      message: 'Get tour by provider id success',
      data: data,
    };
  }

  @Put()
  updateTourStatus(@GetCurrentUser('id') userId: string) {
    return userId;
  }
}
