import { Module } from '@nestjs/common';
import { DataAccessModule } from '../data-access/data-access.module';
import { TourService } from './tour.service';
import { TourController } from './tour.controller';
import { UtilitiesModule } from '../utilities/utilities.module';
import { ValidationService } from '../utilities/validation.service';
import { FirebaseService } from '../utilities/firebase.service';

@Module({
  imports: [DataAccessModule, UtilitiesModule],
  controllers: [TourController],
  providers: [TourService, ValidationService, FirebaseService],
})
export class TourModule {}
