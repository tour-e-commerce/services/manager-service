import { FirebaseService } from '../utilities/firebase.service';

import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { TourAccessService } from '../data-access/tour-access.service';
import { ExcelService } from '../utilities/excel.service';

import { Tour } from '@prisma/client';
import { ValidationService } from '../utilities/validation.service';
import { CreateOneTourDTO } from './dto/create-one-tour.dto';
import { GetAllTourDTO } from './dto/get-all-tour.dto';
import { ResponseDto } from 'src/common/models/interface';
@Injectable()
export class TourService {
  constructor(
    private tourAccessService: TourAccessService,
    private readonly excel: ExcelService,
    private firebaseService: FirebaseService,
    private validationService: ValidationService,
  ) {}
  async addOneTour(
    tour: CreateOneTourDTO,
    userID: string,
    images: Array<Express.Multer.File>,
  ) {
    try {
      console.log('cai nay chay ne');
      console.log(userID);
      // const tempUID = uuidParse(userID);
      const provider = await this.validationService.isProviderCreatedOrThrow(
        userID,
      );
      const createOneTourData = await this.tourAccessService.createOne(
        tour,
        provider.id,
      );

      const imagesData = await this.firebaseService.uploadMultiImage(
        images,
        userID,
      );
      const tourWithImages = this.tourAccessService.updateTourImages(
        createOneTourData.id,
        imagesData,
      );
      return tourWithImages;
    } catch (error) {
      throw new HttpException(
        {
          error,
        },
        500,
      );
    }
  }

  async getTourById(id: string): Promise<ResponseDto<Tour>> {
    try {
      const tour = await this.tourAccessService.getOne(id);

      return {
        status: HttpStatus.OK,
        message: 'get tour successfully',
        data: tour,
      };
    } catch (error) {
      throw new HttpException(
        {
          error,
        },
        500,
      );
    }
  }
  handleCsv(file: Express.Multer.File) {
    return this.excel.excelToJson(file.buffer);
  }
  async getTourByProviderId(
    getAllTourDTO: GetAllTourDTO,
    providerUserID: string,
  ): Promise<Tour[]> {
    const provider = await this.validationService.isProviderCreatedOrThrow(
      providerUserID,
    );
    const data = await this.tourAccessService.getAllByUserId(
      getAllTourDTO,
      provider.id,
    );
    return data;
  }
  async updateTourStatus(userId: string) {
    console.log(userId);
    // const check = await this.tourAccessService.getAllByUserId()
  }
}
