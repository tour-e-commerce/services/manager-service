import { Bucket, Storage } from '@google-cloud/storage';
import { HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ResponseException } from 'src/common/interface/exception.interface';

@Injectable()
export class FirebaseService {
  private storage: Storage;
  private bucket: Bucket;

  constructor(private readonly config: ConfigService) {
    this.storage = new Storage({
      projectId: 'trading-stuff-67e33',
      credentials: {
        client_email:
          'firebase-adminsdk-y33lr@trading-stuff-67e33.iam.gserviceaccount.com',
        client_id: '102842411324887636421',
        private_key:
          '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDT+idOk66leD6c\nWRzIXeI5smpBnKUlY6sPRWAJZpBj1Nn9AO+dO4v1ANQp1cd/xxIBcSxI8VM0hGyk\nmzgbdaPKcEaC8GqN0WJMES2Zr19+85U+P0HyK0KkcqRdQaAb1esXL9tsWBJv5v+E\n0F69TqQbAX0qPji3TzcVYIf6dAalQ8DKnHsUW50hW53G6aOsoyoXP++F5BDPanmh\nrBrhtfg2LG0nSrVqYoPA6HMliu6cUXIa7Kw9n8HfYV/D3kmU3y3AB1NV2zTr/FFd\nR1ZvNdX3H4lCeKmwTT25RvFf4SSHc7zOEkLMyBkJufv2H7Mi/sMQTXO+H62aCm3F\n7fnZTpPVAgMBAAECggEAWZq5uN52id29YavNnekJsuf3QIrtyFNMQOVLhk3DpV+Z\nUfhuUWGVdNiH1rNWZG4K728ekCrqyB1eK2qtmfXtaapyM/FaqV4kFhGA3CEk1e4z\nXR+ySaL9xbPsRLtz6bBQcvm44CCOkpzr0AGgupceiYfkl9G7eclTeIbYlActs/ed\ndEimxaf3aZNCgVs+6z81EzeUklwm9Hob3YFPSVmFf7207Z7SDR3eAv+1zSXguDUd\ny25M6CNl+nOiCMIiEpApfStmNRwdSYm1jzg6hIx5JYRE4rV68JucaUK5vmnplwq0\nhvM1SsKEV4ddj1Jzfyab9I8wky42pXN+v/jU1qTZ9QKBgQD6mVWp1tlFIHbCXidw\nx4VBWIk45hcKZhyTloDnhW0Inql6MTqXLGnXHXX/hZ016E6j3hkXQ/2TN2Po4WRd\nvXegR7g7/PafivW1MJ34rsAXmeHKM67yBAOJOn7bRpf8Ip/9qjf8hY5VnbugJsrp\nBadNkEsVjU03pCUCQkvNzGd1KwKBgQDYi7mwrovE0a0Wbyoxbi0k9r6NNQjKyIym\ndfIJ2oa7Jb7EsGhVpqS/XvcrxNus38I/s4ZQ20ieXH7n9i980KptqOhidiycyxxy\nAk2phRGj187vbLoyMA7Gm8mVU2xdkPfTtqMdDhKUw6VgnjdB4UsfFkh1b/UuK9fV\ngxB9zM+a/wKBgQCeBIQkxUpZsX/RTieWkOkw7FAS5duPfhX05ok/9D1tmwaI+3PT\nkXjn4TbfufQI4wR7hffSWNgTtXC1jzKHfYpqMe81PGyS67/F0fM6tY6BE1Jx+ycM\nioVkSdNmF1yM65RxEp2dCOZYjE6cTPnT9/GoEh2WaHRRapq/kuhTy9lM4wKBgQC+\ncVv0w/1OMm08TrXX1SLE4HBDNcIuvWPdPduIyPQ3AQIeb1rDARTg7T8J8k6UYhGu\n/k8BK2lZRNWjYULwTX+lg6gkd/PSTVsN9XKhxNL3umNW89NpMhS8nOQTfNPQEh+y\nxWv8sJSs2sfGCj9kpCPyCiHdjoIdgheGwoatbIOktwKBgANwr35hVZSb4Vkec0x3\n5ACoC4SO3swqodFNqT0s2FkKV9/OzZiJmRQ02KEIp9zNObJk2fABeN7vWO6Ccmaq\nRrjdiwpWw7mWISLP7uJ/TnmBZqmjI58mmpcDcwo1DMq1XzwNu94pE7BCpE0N2sav\nfvP05yuTmru5rpzT6M1DnJaP\n-----END PRIVATE KEY-----\n',
      },
    });
    this.bucket = this.storage.bucket('trading-stuff-67e33.appspot.com');
  }

  async uploadFile(folder: string, file: Express.Multer.File) {
    console.log('this run');
    try {
      const key = `${folder}/${file.originalname}`;

      const fileUpload = this.bucket.file(key);

      const blobStream = fileUpload.createWriteStream({
        metadata: {
          contentType: file.mimetype,
        },
      });

      blobStream.on('error', error => {
        console.log(error);
        throw new Error('images was not saved to firebase');
      });

      // Resolve the public URL after the upload is finished
      return new Promise<string>((resolve, reject) => {
        blobStream.on('finish', async () => {
          const file = this.bucket.file(key);

          const neverExpireDate = new Date('9999-12-31T23:59:59.999Z');

          const publicUrl = await file.getSignedUrl({
            action: 'read',
            expires: neverExpireDate,
          });

          resolve(publicUrl[0]); // The getSignedUrl() returns an array, so we return the first URL
        });

        // Reject the promise on any stream errors
        blobStream.on('error', reject);

        blobStream.end(file.buffer);
      });
    } catch (error) {
      throw new ResponseException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        error,
      });
    }
  }

  async getImage(folder: string, fileName: string): Promise<string> {
    const key = `${folder}/${fileName}`;

    const file = this.bucket.file(key);

    try {
      const neverExpireDate = new Date('9999-12-31T23:59:59.999Z');

      const publicUrl = await file.getSignedUrl({
        action: 'read',
        expires: neverExpireDate,
      });

      return publicUrl[0];
    } catch (error) {
      console.log(error);
      throw new ResponseException({
        status: HttpStatus.BAD_REQUEST,
        error,
        message: 'Failed to get the image from Firebase',
      });
    }
  }

  async uploadMultiImage(files: Array<Express.Multer.File>, id: string) {
    const uploadedUrls = [];

    for (const file of files) {
      try {
        const url = await this.uploadFile(id, file);
        uploadedUrls.push(url);
      } catch (error) {
        console.error(error);
      }
    }

    return uploadedUrls;
  }

  async deleteImage(folder: string, fileName: string) {
    const filePath = `${folder}/${fileName}`;
    const file = this.bucket.file(filePath);

    await file.delete();

    console.log(`File ${filePath} deleted successfully.`);
  }
}
