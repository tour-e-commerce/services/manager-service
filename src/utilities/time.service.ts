import { Injectable } from '@nestjs/common';

@Injectable()
export class TimeService {
  constructor() {}
  makeUniqueTimeStringArray(timeStrings: string[]): string[] {
    const uniqueTimesSet = new Set<string>();

    timeStrings.forEach(timeString => {
      uniqueTimesSet.add(timeString);
    });

    const uniqueTimesArray = Array.from(uniqueTimesSet);

    return uniqueTimesArray;
  }
  isUniqueTimeStringArray(timeStrings: string[]): boolean {
    const uniqueTimesSet = new Set<string>();

    for (const timeString of timeStrings) {
      if (uniqueTimesSet.has(timeString)) {
        return false;
      }
      uniqueTimesSet.add(timeString);
    }

    return true;
  }
  validateOpeningTime(timeStrings: string[]): boolean {
    if (timeStrings.length % 2 !== 0) {
      return false;
    }
    return true;
  }

  validateAvailabilityTimeSpan(startDate: Date, endDate: Date): boolean {
    const start = new Date(startDate);
    const end = new Date(endDate);
    if (start >= end) {
      console.log('cai nay');
      return false;
    } else {
      console.log('cai kia');
      const currentDate = new Date();
      console.log(start);
      console.log(currentDate);

      currentDate.setDate(currentDate.getDate() - 1);
      console.log(start > currentDate);
      if (start > currentDate) return true;
    }

    return false;
  }
  isTimeBetween(startDate: Date, endDate: Date, selectedDate: Date): boolean {
    const start = new Date(startDate);
    const end = new Date(endDate);
    const selected = new Date(selectedDate);

    if (start >= end) {
      return false;
    }

    return selected >= start && selected <= end;
  }

  dateToTimeStringFormat(timeStrings: Date[]): string[] {
    const timeOnlyArray = timeStrings.map(dateTimeString => {
      const dateObj = new Date(dateTimeString);
      const hours = String(dateObj.getUTCHours()).padStart(2, '0');
      const minutes = String(dateObj.getUTCMinutes()).padStart(2, '0');
      return `${hours}:${minutes}`;
    });
    return timeOnlyArray;
  }
}
