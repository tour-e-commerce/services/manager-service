import { IsString, IsOptional } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class GetAllVehicleDTO {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  search?: string;
}
