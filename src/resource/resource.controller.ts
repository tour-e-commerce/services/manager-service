import { GetAllTagDto } from '../tour/dto/get-all-tag.dto';
import { Controller, Get, HttpStatus, Query } from '@nestjs/common';
import { ResourceService } from './resource.service';
import { ApiTags } from '@nestjs/swagger';
import { GetAllVehicleDTO } from './dto/get-all-vehicle.dto';
@ApiTags('Resource')
@Controller('resource')
export class ResourceController {
  constructor(private readonly resourceService: ResourceService) {}

  @Get('tag')
  async GetAllTag(@Query() getAllTagDTO: GetAllTagDto) {
    const data = await this.resourceService.getAllTag(getAllTagDTO);
    return {
      status: HttpStatus.OK,
      message: 'Get All Tag Success',
      data: data,
    };
  }
  @Get('vehicle')
  async GetAllVehicle(@Query() getAllVehicleDTO: GetAllVehicleDTO) {
    const data = await this.resourceService.getAllVehicle(getAllVehicleDTO);
    return {
      status: HttpStatus.OK,
      message: 'Get All Vehicle Success',
      data: data,
    };
  }
}
