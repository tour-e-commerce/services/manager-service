import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsISO8601,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { SpecialDates, Weekdays } from 'src/common/models/availability';

export class TourAvailabilityDTO {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsString()
  tour_id: string;

  @ApiProperty()
  @IsISO8601()
  validity_date_range_from: Date;

  @ApiProperty()
  @IsISO8601()
  validity_date_range_to: Date;

  @ApiProperty()
  @IsOptional()
  @IsArray()
  @ValidateNested()
  @Type(() => Weekdays)
  weekdays?: Weekdays[];

  @ApiProperty()
  @IsOptional()
  @IsArray()
  @ValidateNested()
  @Type(() => SpecialDates)
  specialDates?: SpecialDates[];
}
