import { IsEnum, IsOptional, IsString } from 'class-validator';
import { AvailabilityStatusSystemEnum } from 'src/common/models/availability';

export class GetAllAvailabilityDTO {
  @IsOptional()
  @IsEnum(AvailabilityStatusSystemEnum)
  status: AvailabilityStatusSystemEnum;

  @IsOptional()
  @IsString()
  tourId: string;
}
