import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { ROLE } from 'src/common/constants/enum';
import { GetCurrentUser } from 'src/common/decorators/get-current-user.decorator';
import { Roles } from '../common/decorators/roles.decorator';
import { AtGuard } from '../common/guards';
import { RolesGuard } from '../common/guards/role.guard';
import { AvailabilityService } from './availability.service';
import { TourAvailabilityDTO } from './dto/create-tour-availability.dto';
import { GetAllAvailabilityDTO } from './dto/get-all-availability.dto';

@ApiTags('Availability')
@Controller('availability')
@Roles(ROLE.PROVIDER)
@UseGuards(AtGuard, RolesGuard)
@ApiBearerAuth()
export class AvailabilityController {
  constructor(private readonly availabilityService: AvailabilityService) {}

  @Post()
  async createAvailability(
    @Body() createAvailDTO: TourAvailabilityDTO,
    @GetCurrentUser('id') providerUserID: string,
  ) {
    const data = await this.availabilityService.createOneAvailability(
      createAvailDTO,
      providerUserID,
    );
    return { status: 201, message: 'Create Availability Success', data: data };
  }

  @Get('/detail/:availability_id')
  async getAvailabilityById(
    @Param('availability_id') id: string,
    @GetCurrentUser('id') providerUserID: string,
  ) {
    const data = await this.availabilityService.getAvailabilityById(
      +id,
      providerUserID,
    );
    return { status: 200, message: 'Get Availability success', data: data };
  }

  @Get('/all/')
  async getAllAvailabilityByProviderId(
    @GetCurrentUser('id') providerUserID: string,
    @Query() query: GetAllAvailabilityDTO,
  ) {
    console.log({ query });
    const data = await this.availabilityService.getAllByProviderId(
      providerUserID,
      query.status,
      query.tourId,
    );
    return { status: 200, message: 'Get all availability success', data: data };
  }

  @Delete('/delete/:availability_id')
  async deleteAvailability(
    @Param('availability_id') id: string,
    @GetCurrentUser('id') providerUserID: string,
  ) {
    const data = await this.availabilityService.deleteAvailability(
      providerUserID,
      +id,
    );
    return { status: 200, message: 'Delete availability success', data: data };
  }

  @Patch('/deactivate/:availability_id')
  async disableAvailability(
    @Param('availability_id') id: string,
    @GetCurrentUser('id') providerUserID: string,
  ) {
    const data = await this.availabilityService.deactivateAvailability(
      providerUserID,
      +id,
    );
    return { status: 200, message: 'Deactivated availability', data: data };
  }

  @Patch('/activate/:availability_id')
  async activateAvailability(
    @Param('availability_id') id: string,
    @GetCurrentUser('id') providerUserID: string,
  ) {
    const data = await this.availabilityService.activateAvailability(
      providerUserID,
      +id,
    );
    return {
      status: HttpStatus.OK,
      message: 'Activated Availability success',
      data: data,
    };
  }

  @Patch('/update/:availability_id')
  async updateAvailability(
    @Param('availability_id') id: string,
    @Body() dto: TourAvailabilityDTO,
    @GetCurrentUser('id') providerUserID: string,
  ) {
    const data = await this.availabilityService.updateOneAvailabilityById(
      dto,
      +id,
      providerUserID,
    );
    return {
      status: HttpStatus.OK,
      message: 'Activated Availability success',
      data: data,
    };
  }
}
