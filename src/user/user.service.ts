import {
  ForbiddenException,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { User } from '@prisma/client';
import * as argon from 'argon2';

import { ResponseDto } from 'src/common/models/interface';
import { UserDataAccess } from '../data-access/user-access.service';
import { FirebaseService } from '../utilities/firebase.service';
import { ChangeUserPasswordDto } from './dto/change-user-password.dto';
import { GetUserPaginationDto } from './dto/get-user-pagination.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UserService {
  constructor(
    private readonly userDataAccess: UserDataAccess,
    private readonly firebaseService: FirebaseService,
  ) {}
  async getAllUsers(dto: GetUserPaginationDto) {
    const users = await this.userDataAccess.getAllUsers(dto);

    return users;
  }

  async getProfile(userId: string): Promise<User> {
    const currentUser = await this.userDataAccess.getUniqueUserById(userId);

    delete currentUser.password;
    delete currentUser.refresh_token;

    return currentUser;
  }

  async updateProfile(userId: string, dto: UpdateUserDto) {
    const updateUser = await this.userDataAccess.updateUserProfile(userId, {
      fullName: dto.fullName,
      gender: dto.gender,
      dob: dto?.dob,
      phoneNumber: dto.phoneNumber,
    });
    return updateUser;
  }

  async changePassword(userId: string, dto: ChangeUserPasswordDto) {
    const user = await this.userDataAccess.getUniqueUserById(userId);

    const oldPwMatch = await argon.verify(user.password, dto.oldPassword);

    if (!oldPwMatch) throw new ForbiddenException('Incorrect password');

    const hash = await argon.hash(dto.newPassword);

    const updateUser = await this.userDataAccess.updateUserPassword(
      userId,
      hash,
    );

    delete updateUser.password;
    delete updateUser.refresh_token;

    return updateUser;
  }

  async updateBannerImage(
    file: Express.Multer.File,
    userId: string,
  ): Promise<ResponseDto<string>> {
    try {
      const url = await this.firebaseService.uploadFile('banner', file);

      await this.userDataAccess.updateUserProfile(userId, {
        bannerImageUrl: url,
      });

      return {
        status: HttpStatus.ACCEPTED,
        message: 'update banner success',
        data: url,
      };
    } catch (error) {
      throw new HttpException('error when updateAvatar', 400, {
        cause: error,
      });
    }
  }

  async updateAvatar(
    file: Express.Multer.File,
    userId: string,
  ): Promise<ResponseDto<string>> {
    try {
      const url = await this.firebaseService.uploadFile('avatar', file);

      await this.userDataAccess.updateUserProfile(userId, {
        avatarImageUrl: url,
      });

      return {
        status: HttpStatus.ACCEPTED,
        message: 'update avatar success',
        data: url,
      };
    } catch (error) {
      throw new HttpException('error when updateAvatar', 400, {
        cause: error,
      });
    }
  }
}
