import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { DataAccessModule } from '../data-access/data-access.module';
import { UtilitiesModule } from '../utilities/utilities.module';

@Module({
  controllers: [UserController],
  providers: [UserService],
  imports: [DataAccessModule, UtilitiesModule],
})
export class UserModule {}
