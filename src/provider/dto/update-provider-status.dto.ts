import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsOptional } from 'class-validator';
import { ProviderStatus } from '../../common/models/provider';

export class UpdateProviderStatusDto {
  @IsOptional()
  @ApiProperty()
  @IsEnum(ProviderStatus)
  status: string;
}
