import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { ProviderDataAccess } from '../data-access/provider-access.service';

import { FirebaseService } from '../utilities/firebase.service';

import { Provider } from '@prisma/client';
import { ResponseDto } from 'src/common/models/interface';
import { ValidationService } from '../utilities/validation.service';
import { CreateProviderDto } from './dto/create-provider.dto';
import { UpdateProviderDTO } from './dto/update-provider.dto';

@Injectable()
export class ProviderService {
  constructor(
    private readonly providerDataService: ProviderDataAccess,
    private readonly firebaseService: FirebaseService,

    private validationService: ValidationService,
  ) {}

  async createProvider(
    userId: string,
    dto: CreateProviderDto,
    file: Express.Multer.File,
  ): Promise<ResponseDto<Provider>> {
    const check = await this.providerDataService.getProviderByUserId(userId);

    if (check) throw new BadRequestException('provider already exist');

    try {
      const imageUrl = await this.firebaseService.uploadFile('license', file);
      const provider = await this.providerDataService.createProvider(userId, {
        ...dto,
        file: imageUrl,
      });

      return {
        status: HttpStatus.CREATED,
        message: 'create provider successfully',
        data: provider,
      };
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException(error);
    }
  }

  async getInformation(userId: string): Promise<ResponseDto<Provider>> {
    const provider = await this.providerDataService
      .getProviderByUserId(userId)
      .catch(error => {
        throw new HttpException(error, 500);
      });

    if (!provider) throw new NotFoundException('Provider not register');
    return {
      status: HttpStatus.OK,
      message: 'get provider successfully',
      data: provider,
    };
  }

  async updateProvider(userId: string, dto: UpdateProviderDTO): Promise<any> {
    // Promise<ResponseDto<Provider>>
    const check = await this.providerDataService.getProviderByUserId(userId);

    if (!check)
      throw new NotFoundException('provider is not exist or not registered');

    const { id } = check;

    try {
      const provider = await this.providerDataService.updateProvider(id, dto);
      return {
        status: HttpStatus.OK,
        message: 'update provider successfully',
        data: provider,
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async patchProviderAvatar(puid: string, avatar: Express.Multer.File) {
    const imgURL = await this.firebaseService.uploadFile('avatar', avatar);
    await this.validationService.isProviderCreatedOrThrow(puid);
    return await this.providerDataService.patchProviderAvatar(puid, imgURL);
  }
  async patchProviderBanner(puid: string, banner: Express.Multer.File) {
    await this.validationService.isProviderCreatedOrThrow(puid);
    const imgURL = await this.firebaseService.uploadFile('banner', banner);

    return await this.providerDataService.patchProviderBanner(puid, imgURL);
  }
}
